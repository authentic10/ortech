# Mise en place

## Préparation 
- Installer un environnement de virualisation avec
`pip install virtualenv`
- Créer et activer un environnement de virtualisation
```batch
python -m venv venv
.\venv\Script\activate
```
- Installer Flask avec
`pip install flask`

## Lancement de l'application

- Définir les variables d'environnement nécessaires
```
set FLASK_ENV=development
set FLASK_APP=app
```

- Démarrer le serveur avec
`flask run`


